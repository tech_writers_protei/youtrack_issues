from importlib import import_module
from json import dumps
import os.path as p
from os import mkdir
import warnings
from logging import NullHandler, getLogger, basicConfig
from subprocess import run, CalledProcessError, TimeoutExpired
from sys import executable
from typing import Any

from loguru import logger

import __version__ as v

__title__ = v.__title__
__description__ = v.__description__
__url__ = v.__url__
__version__ = v.__version__
__build__ = v.__build__
__author__ = v.__author__
__license__ = v.__license__

from sending_emails.init_logger import HandlerType, LoggingLevel, LoggerConfiguration, InterceptHandler

_parent_path: str = p.dirname(p.dirname(p.abspath(__file__)))
__path__.append(_parent_path)
_module_path: str = p.join(_parent_path, "sending_emails")
__path__.append(_module_path)

warnings.simplefilter("ignore")

for _package in ("loguru",):
    try:
        import_module(_package)
    except (ModuleNotFoundError, ImportError):
        try:
            run([executable, "-m", "pip", "install", _package], timeout=30, check=True)
        except CalledProcessError as e:
            print(f"{e.__class__.__name__}\nКод ответа {e.returncode}\nКоманда {e.cmd}\nОшибка {e.output}")
            print(f"Не удалось импортировать пакет `{_package}`")
            raise
        except TimeoutExpired as e:
            print(f"{e.__class__.__name__}\nТаймаут {e.timeout}\nКоманда {e.cmd}\nОшибка {e.output}")
            print(f"Не удалось импортировать пакет `{_package}`")
            raise
        except OSError as e:
            print(f"{e.__class__.__name__}\nФайл {e.filename}\nОшибка {e.strerror}")
            print(f"Не удалось импортировать пакет `{_package}`")
            raise
        else:
            print(f"Установлен пакет `{_package}`")
            import_module(_package)

_issues_json_file: str = p.join(_module_path, "issues.json")

if not p.exists(_issues_json_file):
    text: dict[str, list[str]] = {
        "andrianova": [],
        "babenko": [],
        "bochkova": [],
        "bozhok": [],
        "brovko": [],
        "chursin": [],
        "demyanenko": [],
        "fesenko": [],
        "kuksina": [],
        "lyalina": [],
        "matyushina": [],
        "mazyarova": [],
        "mikhailova-m": [],
        "nikitina": [],
        "rotmanova": [],
        "safonova": [],
        "sergeev-d": [],
        "sobolev-p": [],
        "sukhova": [],
        "vykhodtsev": [],
        "tarasov-a": []
    }

    with open(_issues_json_file, "w") as file:
        file.write(dumps(text, indent=2))

_log_folder: str = p.join(_module_path, "logs")

if not p.exists(_log_folder):
    mkdir(_log_folder)

getLogger(__name__).addHandler(NullHandler())

handlers: dict[HandlerType, LoggingLevel] = {
    "stream": "INFO",
    "file_rotating": "DEBUG"
}

file_name: str = "sending_emails"

logger_configuration: LoggerConfiguration = LoggerConfiguration(file_name, handlers)

stream_handler: dict[str, Any] = logger_configuration.stream_handler()
rotating_file_handler: dict[str, Any] = logger_configuration.rotating_file_handler()

logger.configure(handlers=[stream_handler, rotating_file_handler])

basicConfig(handlers=[InterceptHandler()], level=0)
