from os import environ
from smtplib import SMTP, SMTPConnectError, SMTPServerDisconnected, SMTPNotSupportedError, SMTPAuthenticationError, \
    SMTPResponseException, SMTPException, SMTPHeloError, SMTPSenderRefused, SMTPDataError, SMTPRecipientsRefused
from ssl import create_default_context, SSLContext
from typing import Iterable

from loguru import logger

from custom_exceptions import MailServerRunError, MessageGenerationError
from enum_objects import MessageYoutrack, MessageYoutrackEmpty


class MailServer:
    """
    Specifies the mail SMTP server.

    Params:
        host --- the SMTP server DNS name, str;\n
        _port --- the SMTP server _port, int;\n
        _user --- the login to authenticate in the server, str;\n
        _password --- the _password to authenticate in the server, str;\n

    Functions:
        server() --- run the SMTP server;\n
        send_email(message: MessageYoutrack) --- send the message to the mailbox.
    """

    def __init__(
            self,
            host: str = None,
            port: int = None,
            user: str = None,
            password: str = None):
        if host is None:
            host = environ.get("SMTP_HOST")

        if port is None:
            port = environ.get("SMTP_PORT")

        if user is None:
            user = environ.get("SMTP_USER")

        if password is None:
            password = environ.get("SMTP_PASSWORD")

        self._host: str = host
        self._port: int = port
        self._user: str = user
        self._password: str = password
        self._smtp_server: SMTP | None = None

    def __repr__(self):
        return f"<{self.__class__.__name__}(host={self._host}, _port={self._port}, _user={self._user}, " \
               f"_password={self._password})> "

    def __str__(self):
        return f"Mail server: {self._host}:{self._port}, SMTP server active: {self._smtp_server is None}"

    def __bool__(self):
        return self._smtp_server is not None

    def server(self):
        """Runs the SMTP server to send emails. Further the server is used as a context manager."""
        context: SSLContext = create_default_context()
        smtp_server: SMTP = SMTP(self._host, self._port)
        try:
            logger.info(f"Попытка подключения к SMTP-серверу: {self._host}:{self._port}")
            smtp_server.connect(self._host, self._port)
            # set the connection
            smtp_server.ehlo()
            smtp_server.starttls(context=context)
            smtp_server.ehlo()
            # authenticate
            smtp_server.login(self._user, self._password)
            logger.info(f"Авторизация прошла успешно")

        except (SMTPAuthenticationError, SMTPResponseException, SMTPConnectError) as e:
            logger.critical(
                f"{e.__class__.__name__}\n"
                f"SMTP Code: {e.smtp_code}\n"
                f"Error: {e.smtp_error}\n"
                f"{e.strerror}")
            raise MailServerRunError

        except (SMTPException, ConnectionRefusedError, SMTPServerDisconnected, SMTPNotSupportedError) as e:
            logger.critical(
                f"{e.__class__.__name__}\n"
                f"File: {e.filename}\n"
                f"{e.strerror}")
            raise MailServerRunError

        except RuntimeError as e:
            logger.critical(f"{e.__class__.__name__}\n{str(e)}")
            raise MailServerRunError

        else:
            logger.info("SMTP-соединение установлено")
            self._smtp_server = smtp_server
            return

    def send_emails(self, messages: Iterable[MessageYoutrack] = None):
        """
        Sends the messages to the users.

        :param messages: the message to deliver
        :type messages: Iterable[MessageYoutrack]
        :return: None.
        """
        if not self._smtp_server:
            logger.critical("SMTP-сервер не активен")
            raise MailServerRunError

        if messages is None:
            logger.critical("Не сформированы сообщения для отправки")
            raise MessageGenerationError

        if all([isinstance(message, MessageYoutrackEmpty) for message in messages]):
            logger.warning("Нет сообщений для отправки")
            return

        with self._smtp_server as smtp_server:
            for message in filter(lambda x: not isinstance(x, MessageYoutrackEmpty), messages):
                try:
                    smtp_server.sendmail(message.sender_email, message.recipient_email, message.message())

                except (SMTPHeloError, SMTPSenderRefused, SMTPDataError) as e:
                    logger.error(
                        f"{e.__class__.__name__}\n"
                        f"SMTP Code: {e.smtp_code}\n"
                        f"Error: {e.smtp_error}\n{e.strerror}")
                    raise MailServerRunError

                except (SMTPNotSupportedError, SMTPRecipientsRefused) as e:
                    logger.error(f"{e.__class__.__name__}\n{e.strerror}")
                    raise MailServerRunError

                else:
                    logger.info(
                        f"Сообщение отправлено пользователю {message.recipient_name} "
                        f"на почтовый ящик {message.recipient_email}")

        # stop the SMTP server
        self._smtp_server.close()
        self._smtp_server = None
        logger.info("SMTP-сервер отключен")
        return
