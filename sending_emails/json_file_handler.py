import json
from pathlib import Path

from loguru import logger


class JSONFile:
    content = None

    def __init__(self, file_name: str = "issues.json"):
        self._file_name: str = file_name
        self._path: Path = Path(__file__).with_name(self._file_name)

    def __str__(self):
        return f"{self.__class__.__name__}: {self._file_name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}(file_name={self._file_name})>"

    def read(self):
        try:
            with open(self._path, "r") as file:
                content: dict[str, list[str]] = json.load(file)

        except FileNotFoundError as e:
            logger.error(f"{e.__class__.__name__}, file: {e.filename}\n{e.strerror}")
            return

        except IsADirectoryError as e:
            logger.error(f"{e.__class__.__name__}, file: {e.filename}\n{e.strerror}")
            return

        except PermissionError as e:
            logger.error(f"{e.__class__.__name__}, file: {e.filename}\n{e.strerror}")
            return

        except OSError as e:
            logger.error(f"{e.__class__.__name__}, file: {e.filename}\n{e.strerror}")
            return

        else:
            self.content = content
            return

    def write(self, text: dict[str, list[str]] = None):
        if text is None:
            logger.debug(f"No content to write")
            return

        try:
            with open(self._path, "w", encoding="utf8") as file:
                file.write(json.dumps(text, indent=2, sort_keys=True, ensure_ascii=False))

        except FileNotFoundError | IsADirectoryError | PermissionError | OSError as e:
            logger.error(f"{e.__class__.__name__}, файл: {e.filename}\n{e.strerror}")
            return

        else:
            logger.debug(f"Data is written to the file {self._file_name}")

    def __getitem__(self, item):
        return self.content.__getitem__(item)

    def __setitem__(self, key, value):
        self.content.__setitem__(key, value)

    def __iter__(self):
        return iter(self.content)

    def __len__(self):
        return sum(len(self[user]) for user in self.content.keys())

    def __bool__(self):
        return len(self) > 0

    def __contains__(self, item):
        return item in self.content.keys()

    def nullify(self):
        for login in self:
            self[login] = []

        self.write(self.content)

    @property
    def file_name(self):
        return self._file_name


json_file: JSONFile = JSONFile()
