import json
from functools import cached_property
from typing import Iterable

from loguru import logger

from enum_objects import _params, _headers_request, OptStr, MessageAttributes, MessageYoutrack, MessageYoutrackEmpty
from http_requests import CustomScheme, CustomPort, CustomHTTPRequest, CustomPreparedRequest, ResponseParser
from json_file_handler import JSONFile
from mail_server import MailServer
from message_processing import MessageProcessor

__all__ = ["User", "FullUserProcessor"]


class FullUserProcessor:
    _sender_email: str = "tarasov-a@protei.ru"
    test_message_attributes: MessageAttributes = MessageAttributes(
        f"Оповещение о задачах <{_sender_email}>",
        "Тестирование Задачи YouTrack, Исполнители_Doc/Doc_ST/Archive/Archive_ST",
        "text/plain; charset=UTF-8; format=flowed",
        "8bit",
        "ru",
        "1.0",
        f"<{_sender_email}>"
    )
    message_attributes: MessageAttributes = MessageAttributes(
        f"Оповещение о задачах <{_sender_email}>",
        "Задачи YouTrack, Исполнители_Doc/Doc_ST/Archive/Archive_ST",
        "text/plain; charset=UTF-8; format=flowed",
        "8bit",
        "ru",
        "1.0",
        f"<{_sender_email}>"
    )
    _user_names: dict[str, str] = {
        "bochkova": "Софья Бочкова",
        "brovko": "Максим Бровко",
        "chursin": "Алексей Чурсин",
        "demyanenko": "Светлана Демьяненко",
        "fesenko": "Виктор Фесенко",
        "kuksina": "Ольга Куксина",
        "lyalina": "Кристина Лялина",
        "matyushina": "Вероника Мозглякова",
        "mazyarova": "Дарья Мазярова",
        "nikitina": "Наталья Никитина",
        "sobolev-p": "Павел Соболев",
        "vykhodtsev": "Алексей Выходцев",
        "rotmanova": "Ольга Ротманова",
        "tarasov-a": "Андрей Тарасов",
        "andrianova": "Валерия Андрианова",
        "bozhok": "Влада Божок",
        "sergeev-d": "Дмитрий Сергеев",
        "sukhova": "Лидия Сухова",
        "babenko-v": "Валентин Бабенко",
        "safonova": "Софья Сафонова",
        "mikhailova-m": "Марина Михайлова"
    }

    def __init__(
            self,
            file_name: str = "issues.json", *,
            is_test: bool = False):
        self.json_file: JSONFile = JSONFile(file_name)
        self.json_file.read()
        self.is_test: bool = is_test
        self.logins: list[str] = [*self.json_file.content.keys()]
        self.smtp_server: MailServer = MailServer()
        self._users: list[User] = []

    @property
    def users(self):
        return self._users

    @users.setter
    def users(self, value):
        self._users = value

    def __repr__(self):
        return f"<{self.__class__.__name__}(logins={self.logins}, is_test={self.is_test})>"

    def __str__(self):
        return f"{self.__class__.__name__}: logins = {self.logins}, test: {self.is_test}"

    def to_json_file(self):
        content: dict[str, list[str]] = dict()

        for user in self.users:
            content[user.login] = []
            content[user.login].extend(map(lambda x: x.issue, user.responses_youtrack))
        return content

    def send_mails(self, messages: Iterable[MessageYoutrack] | None = None):
        self.smtp_server.send_emails(messages)


class User(FullUserProcessor):
    def __init__(self, file_name: str, login: str, *, is_test: bool = False, is_init: bool = False):
        super().__init__(file_name, is_test=is_test)
        self.login: str = login
        self.is_init: bool = is_init
        logger.debug(f"----------")
        logger.debug(f"Пользователь {self.login}")

        if self.is_test:
            self._recipient_email: str = f"{self._sender_email}"
        else:
            self._recipient_email: str = f"{self.login}@protei.ru"

    def __repr__(self):
        return f"<{self.__class__.__name__}(login={self.login}, is_test={self.is_test})>"

    def __str__(self):
        return f"Пользователь {self._user_names[self.login]}, {self._recipient_email}"

    def message_youtrack(self) -> MessageYoutrack:
        _message_processor: MessageProcessor = MessageProcessor(self.json_file, self.login, self.responses_youtrack)
        _message_text: OptStr = _message_processor.message_text(self.is_init)

        if _message_text is None:
            return MessageYoutrackEmpty()

        msg_attrs: MessageAttributes = self.test_message_attributes if self.is_test else self.message_attributes

        return MessageYoutrack(self._sender_email, self._recipient_email, _message_text, msg_attrs)

    def send_mails(self, messages: Iterable[MessageYoutrack] | None = None):
        return NotImplemented

    @cached_property
    def responses_youtrack(self):
        scheme: str = CustomScheme.HTTPS.value
        web_hook: str = "api/issues"
        host: str = "youtrack.protei.ru"
        port: int = CustomPort.HTTPS.value
        params: tuple[tuple[str, str], ...] = _params(self.login)
        method: str = "GET"
        data: bytes | None = None
        headers: dict[str, str] = _headers_request
        _http_request: CustomHTTPRequest
        _http_request = CustomHTTPRequest(scheme=scheme, web_hook=web_hook, host=host, port=port, params=params)
        _prepared_request: CustomPreparedRequest
        _prepared_request = CustomPreparedRequest.from_custom_request(_http_request, method, data, headers)
        _response: str = _prepared_request.get_response()
        logger.debug(f"Ответ:\n{_response}")
        _json_response: dict[str, object] | list[dict[str, object]] | None = json.loads(_response)
        response_parser: ResponseParser = ResponseParser(_json_response)
        return response_parser.parse()
