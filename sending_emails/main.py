import faulthandler
import os
from typing import Iterable

from loguru import logger

from enum_objects import MessageYoutrack
from init_logger import configure_custom_logging
from json_file_handler import JSONFile
from user_processing import FullUserProcessor, User


def _inspect_env(names: Iterable[str] = None):
    if names is None:
        return

    names: list[str] = [names] if isinstance(names, str) else [*names]

    if all(name in os.environ.keys() for name in names):
        return

    else:
        for _ in names:
            if os.environ.get(_, None) is None:
                logger.error(f"{_} не найден в переменных окружения")


@logger.catch
@configure_custom_logging("sending_emails")
def main(*, is_test: bool = False, is_init: bool = False):
    json_file: JSONFile = JSONFile()
    json_file.read()
    content: dict[str, list[str]] = json_file.content

    logins: list[str] = [*content.keys()]

    full_user_processor: FullUserProcessor = FullUserProcessor(json_file.file_name, is_test=is_test)
    all_messages_youtrack: list[MessageYoutrack] = []

    for login in logins:
        user: User = User(json_file.file_name, login, is_test=is_test, is_init=is_init)
        full_user_processor.users.append(user)
        all_messages_youtrack.append(user.message_youtrack())

    if all_messages_youtrack:
        full_user_processor.smtp_server.server()
        full_user_processor.send_mails(all_messages_youtrack)

    json_file.write(full_user_processor.to_json_file())


if __name__ == "__main__":
    faulthandler.enable()
    main()
    if faulthandler.is_enabled():
        faulthandler.disable()
