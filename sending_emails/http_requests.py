import urllib
from datetime import date
from typing import NamedTuple, Mapping, Iterable
from urllib.parse import quote_plus
from urllib.request import Request

from loguru import logger
from numpy.core import datetime64
from numpy import isnat
from urllib3 import HTTPResponse

from custom_exceptions import RequiredAttributeMissingError, NoResponseError
from enum_objects import CustomScheme, CustomPort

_protocol = "HTTPS"


def convert_long_date(long) -> datetime64:
    """
    Convert the long value to the datetime64.

    :param long: the timestamp
    :return: the date associated with the timestamp.
    :rtype: datetime64
    """
    if long is None:
        return datetime64("NaT")

    else:
        return datetime64(long, "ms").astype("datetime64[D]")


def replace_unicode_chars(text: str | None = None):
    # specify the symbols to replace
    conversion_table: dict[str, str] = {"\u2011": "-", "\u2010": "-", "\t": " "}
    if text is None:
        return

    if any(key in text for key in conversion_table):
        for k, v in conversion_table.items():
            if k in text:
                text = text.replace(k, v)

    return text


class ResponseYoutrack(NamedTuple):
    """
    Define the Issue entity from the YouTrack.

    Params:
        issue --- the issue name, idReadable, {project}-{id};\n
        state --- the issue state, state;\n
        summary -- the issue short description, summary;\n
        parent --- the parent issue name, parent;\n
        deadline --- the issue deadline, deadline;\n

    state values:\n
    Active/New/Paused/Done/Test/Verified/Discuss/Closed/Review/Canceled\n
    """
    issue: str
    state: str
    summary: str
    deadline: datetime64 = datetime64("NaT")
    priority: str = "Basic"

    def __str__(self):
        if isnat(self.deadline):
            deadline_string: str = ""

        else:
            deadline_string: str = f", deadline = {self.deadline.item().isoformat()}"

        return f"Issue: issue = {self.issue}, state = {self.state}, priority = {self.priority}, " \
               f"summary = {self.summary}{deadline_string}"

    def __repr__(self):
        return f"<Issue(issue={self.issue}, state={self.state}, summary={self.summary}, deadline={self.deadline})>"

    def __format__(self, format_spec):
        if format_spec == "output":
            if isnat(self.deadline):
                deadline_string = ""

            else:
                deadline_string = f", дедлайн: {self.deadline.item().isoformat()}"

            return f"Задача: {self.issue} ({self.hyperlink}), статус: {self.state}, приоритет: {self.priority}.\n" \
                   f"Описание: {self.summary}{deadline_string};"

        elif format_spec == "log":
            return f"Задача: {self.issue}, {self.hyperlink}"

    def __hash__(self):
        return hash(self.issue)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.issue == other.issue

        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.issue != other.issue

        else:
            return NotImplemented

    def __lt__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented

        if self.issue == other.issue:
            return False

        if self._priority_int < other._priority_int:
            return True

        elif self._priority_int > other._priority_int:
            return False

        if self._deadline_date < other._deadline_date:
            return True

        elif self._deadline_date > other._deadline_date:
            return False

        return self._issue_number < other._issue_number

    def __gt__(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented

        if self.issue == other.issue:
            return False

        if self._priority_int > other._priority_int:
            return True

        elif self._priority_int < other._priority_int:
            return False

        if self._deadline_date > other._deadline_date:
            return True

        elif self._deadline_date < other._deadline_date:
            return False

        return self._issue_number > other._issue_number

    @property
    def _priority_conversion(self) -> dict[str, int]:
        """
        Mapping of the priorities and integers.

        :return: the dictionary.
        :rtype: dict[str, int]
        """
        return {
            "Critical": 30,
            "Important": 20,
            "Basic": 10,
            "Low": 0
        }

    @property
    def hyperlink(self) -> str:
        """
        Specifies the hyperlink to the issue in the Youtrack.

        :return: the active hyperlink.
        :rtype: str
        """
        return f"https://youtrack.protei.ru/issue/{self.issue}"

    @property
    def _deadline_date(self) -> date:
        """
        Converts the deadline to the date format. If the deadline is missing, returns the date in the farthest future.\n
        Used for sorting issues.

        :return: the deadline date.
        :rtype: date
        """
        if self.deadline is None or isnat(self.deadline):
            return date(9999, 1, 1)

        else:
            return date.fromisoformat(self.deadline.item().isoformat())

    @property
    def _issue_number(self) -> int:
        """
        Gets the issue number from the full issue id value, <project>-<issue_number>.\n
        Used for sorting issues.

        :return: the issue number.
        :rtype: int
        """
        return int(self.issue.split("-")[1])

    @property
    def _priority_int(self) -> int:
        """
        Maps the issue priority to the integer following the conversion table.\n
        Used for sorting issues.

        :return: the associated priority integer value.
        :rtype: int
        """
        return self._priority_conversion.get(self.priority, 0)


class CustomHTTPRequest:
    identifier = 0

    def __init__(
            self, *,
            scheme: str | None = None,
            web_hook: str | None = None,
            host: str | None = None,
            port: int | None = None,
            params: tuple[tuple[str, str], ...] | None = None):
        if scheme is None:
            scheme: str = CustomScheme[f"{_protocol}"].value

        if port is None:
            port: int = CustomPort[_protocol].value

        self._scheme: str = scheme
        self._web_hook: str = web_hook
        self._host: str = host
        self._port: int = port
        self._params: tuple[tuple[str, str], ...] = params
        self.index: int = self.__class__.identifier

        self.__class__.identifier += 1

    def __str__(self):
        return f"{self.__class__.__name__}: scheme = {self._scheme}, web hook = {self._web_hook}, " \
               f"host = {self._host}, post = {self._port}, params = {self._params}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._scheme}, {self._web_hook}, {self._host}, {self._port}," \
               f" {self._params})>"

    def __hash__(self):
        return hash(self.url())

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.url() == other.url()

        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.url() != other.url()

        else:
            return NotImplemented

    def _get_params(self) -> str:
        if self._params is None:
            return ""

        _params = "&".join([f"{name}={value}" for name, value in self._params])
        return f"?{_params}"

    def _get_web_hook(self) -> str:
        if self._web_hook is not None:
            return self._web_hook.rstrip("/")

        else:
            logger.error("Не указан web hook")
            raise RequiredAttributeMissingError

    def _get_scheme(self) -> str:
        if self._scheme is not None:
            return self._scheme.lower().strip("/")

        else:
            return "https"

    def _get_port(self) -> int:
        if self._port is None:
            return CustomPort[_protocol].value

        else:
            return self._port

    def _get_host(self) -> str:
        if self._host is not None:
            return self._host.strip("/")

        else:
            logger.error("Не указан адрес host")
            raise RequiredAttributeMissingError

    def url(self) -> str:
        _url: str = f"{self._get_scheme()}://{self._get_host()}:{self._get_port()}/{self._web_hook}{self._get_params()}"
        return quote_plus(_url, ":/&?=")

    @property
    def host(self) -> str:
        return self._host


class CustomPreparedRequest(NamedTuple):
    url: str
    data: bytes
    headers: dict[str, str]
    host: str
    unverifiable: bool
    method: str
    index: int

    def __str__(self):
        return f"Соединение #{self.index}:\n{str(self.request().full_url)}"

    def __repr__(self):
        return f"<{self.__class__.__name__}: Connection #{self.index} ({self.url}, {self.data}, " \
               f"{[header for header in self.headers]}, " \
               f"{self.host}, {self.unverifiable}, {self.method})>"

    def __hash__(self):
        return hash((self.host, self.headers, self.data))

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.request() == other.request()

        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.request() != other.request()

        else:
            return NotImplemented

    @classmethod
    def from_custom_request(
            cls,
            custom_http_request: CustomHTTPRequest,
            method: str = None,
            data: bytes = None,
            headers: dict[str, str] = None):
        if headers is None:
            headers = dict()

        if data is None:
            data = b''

        return cls(
            custom_http_request.url(),
            data,
            headers,
            custom_http_request.host,
            False,
            method,
            custom_http_request.index)

    def request(self) -> Request:
        if self.headers is None:
            self.headers = dict()

        return Request(self.url, self.data, self.headers, self.host, self.unverifiable, self.method)

    def get_response(self) -> str:
        response: HTTPResponse
        logger.debug(str(self))

        with urllib.request.urlopen(self.request()) as response:
            return response.read().decode('utf-8')


class ResponseParser:
    def __init__(self, response: Mapping[str, object] | Iterable[Mapping[str, object]] | None = None):
        if isinstance(response, Mapping):
            self._response: dict[str, object] = {**response}

        elif isinstance(response, Iterable):
            self._response: list[dict[str, object]] = [*response]

        elif response is None:
            self._response = None

        else:
            logger.error(f"Response is invalid: {response}")
            raise NoResponseError

    def __str__(self):
        return f"{self.__class__.__name__}: {str(self._response)}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({repr(self._response)})>"

    def parse(self):
        issues: list[ResponseYoutrack] = []

        if self._response is None:
            logger.error("Ответ на запрос пуст или запрос некорректен")
            return issues

        for response_item in self._response:
            issue: str = response_item["idReadable"]
            summary: str = response_item["summary"]
            deadline: datetime64 = datetime64("NaT")
            state: str = ""
            priority: str = "Basic"

            item: dict[str, str | int | dict[str, object] | None]
            for item in response_item["customFields"]:
                # specify the issue state
                if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField"):
                    state = item["value"]["name"]

                # specify the issue deadline
                elif item["$type"] == "DateIssueCustomField":
                    deadline = convert_long_date(item["value"])

                elif item["$type"] == "SingleEnumIssueCustomField":
                    priority = item["value"]["name"]

                else:
                    continue
            issues.append(ResponseYoutrack(issue, state, replace_unicode_chars(summary), deadline, priority))

        _log: str = '\n'.join([str(issue) for issue in issues])
        logger.debug(f"Задачи:\n{_log}")

        return issues
