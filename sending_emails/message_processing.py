from textwrap import dedent
from typing import Iterable

from loguru import logger

from json_file_handler import JSONFile
from enum_objects import OptStr, _sep
from http_requests import ResponseYoutrack

__all__ = ["MessageProcessor"]


class _IssueFilter:
    def __init__(self, json_file: JSONFile):
        self._json_file: JSONFile = json_file
        self.json_file_content: dict[str, list[str]] = dict()

    @property
    def content(self) -> dict[str, list[str]]:
        return self._json_file.content

    def __getitem__(self, item):
        try:
            value: list[str] = self.content.get(item)

        except KeyError and TypeError as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            return

        except OSError as e:
            logger.error(f"{e.__class__.__name__}, file name: {e.filename}, {e.strerror}")
            return

        else:
            return value

    def __setitem__(self, key, value):
        try:
            self.content[key] = []
            self.content[key] = list(*value)

        except (KeyError, TypeError, ValueError) as e:
            logger.error(f"{e.__class__.__name__}, {str(e)}")
            return

        except OSError as e:
            logger.error(f"{e.__class__.__name__}, file name: {e.filename}, {e.strerror}")
            return

    def get_issue_filter_user(self, login: str, responses: Iterable[ResponseYoutrack] | None = None):
        if responses is None:
            responses: list[ResponseYoutrack] = []

        if login not in self.content.keys():
            logger.error(f"Логин {login} не найден в списке пользователей")
            return

        else:
            return _IssueFilterUser(self._json_file, login, responses)


class _IssueFilterUser(_IssueFilter):
    def __init__(
            self,
            json_file: JSONFile,
            login: str,
            responses: Iterable[ResponseYoutrack] | None = None):
        if responses is None:
            responses: list[ResponseYoutrack] = []

        super().__init__(json_file)
        self.login: str = login
        self._responses: list[ResponseYoutrack] = [*responses]

    def __repr__(self):
        return f"<{self.__class__.__name__}(json_file={str(self.json_file_content[self.login])}," \
               f"login={self.login}, responses={self._responses})>"

    def __str__(self):
        return f"Фильтр:\nJSON-файл -- {self.json_file_content[self.login]}, пользователь {self.login}\n" \
               f"Запросы Youtrack: {self._responses}"

    @property
    def _issues_json(self):
        return self.content[self.login]

    @property
    def _issues_youtrack(self) -> set[str] | None:
        return {response.issue for response in self._responses} if self._responses else set()

    def new_responses(self) -> list[ResponseYoutrack] | None:
        if self._new_issues:
            return list(filter(lambda x: x.issue in self._new_issues, self._responses))

    @property
    def _common_issues(self) -> set[str]:
        return set(self._issues_json).intersection(set(self._issues_youtrack))

    @property
    def _new_issues(self) -> set[str]:
        return set(self._issues_youtrack).difference(set(self._issues_json))

    def joined_issues(self) -> list[str]:
        _common: list[str] = [*self._common_issues] if self._common_issues else []
        _new: list[str] = [*self._new_issues] if self._new_issues else []
        return [*self._common_issues, *self._new_issues]


class _MessageTextUser:
    _message_init: str = dedent("""\
        Добрый день, $USERNAME$.
        
        Попытка номер 2 отправки оповещений о назначенных задачах.
        Вкратце, основная идея -- рассылка оповещений при добавлении пользователей в задачу Youtrack в поле 
        Исполнители_*.
        Если были назначены задачи, то придет сообщение подобное этому, в котором будут перечислены ТОЛЬКО НОВЫЕ задачи.
        Если же задачи со вчерашнего дня не добавились, то ничего на почтовый ящик не придет, чтобы не захламлять его 
        лишними сообщениями.
        Обновление статуса, завершение задачи, изменение дедлайна, добавление комментариев и т.п. не активируют 
        рассылку.
        Предложения, замечания и пожелания приветствуются. А теперь по делу:\n
        Текущие незавершенные задачи на YouTrack, назначенные через поле Исполнители_*:\n""")
    _message_start: str = dedent("""\
        Добрый день, $USERNAME$.
        
        Краткая информация о текущих задачах на YouTrack, назначенных через поле Исполнители_*:
        """)
    _message_end: str = dedent("""\
        Обратная связь: tarasov-a@protei.ru / @andrewtar (https://t.me/andrewtar)""")
    _message_end_english: str = dedent("""\
    If the message above is a mess of rubbish characters and not readable, please notify about the problem, 
    thank you.""")
    _sentinel: str = "--------------------"

    def __init__(
            self,
            login: OptStr,
            responses: Iterable[ResponseYoutrack] | None = None):
        if responses is None:
            responses: list[ResponseYoutrack] = []
        self.login: OptStr = login
        self.responses: list[ResponseYoutrack] = [*responses]

    def __repr__(self):
        return f"<{self.__class__.__name__}(login={self.login}, responses={self.responses})>"

    def __str__(self):
        return f"Запросы с Youtrack:\n{[str(response) for response in self.responses]}"

    @property
    def _sorted_messages(self) -> OptStr:
        return "\n".join(format(response, "output") for response in sorted(self.responses))

    def init_message(self):
        _message_structure: tuple[str, ...] = (
            self._message_init,
            self._sorted_messages,
            self._sentinel,
            self._message_end,
            self._message_end_english)
        return "\n".join(_message_structure)

    def message_text(self, is_init: bool = False) -> OptStr:
        first: str = self._message_init if is_init else self._message_start
        _message_structure: tuple[str, ...] = (
            first,
            self._sorted_messages,
            "\n\n",
            self._sentinel,
            self._message_end,
            self._message_end_english)
        return "\n".join(_message_structure)


class _MessageTextUserEmpty(_MessageTextUser):
    def __init__(self):
        super().__init__(None, None)

    def __repr__(self):
        return f"<{self.__class__.__name__}: NullObject>"

    def __str__(self):
        return f"{self.__class__.__name__}: Null Message"

    @property
    def _sorted_messages(self) -> str:
        return "Задач нет.\n"

    def message_text(self, is_init: bool = False) -> OptStr:
        return self.message_text(is_init) if is_init else None


class MessageProcessor:
    def __init__(
            self,
            json_file: JSONFile,
            login: str,
            responses: Iterable[ResponseYoutrack] = None):
        if responses is None:
            responses: list[ResponseYoutrack] = []
        self._json_file: JSONFile = json_file
        self._login: str = login
        self._responses: list[ResponseYoutrack] = responses

    @property
    def _issue_filter_user(self) -> _IssueFilterUser:
        _issue_filter: _IssueFilter = _IssueFilter(self._json_file)
        return _issue_filter.get_issue_filter_user(self._login, self._responses)

    def message_text(self, is_init: bool = False) -> OptStr:
        _responses: list[ResponseYoutrack] | None = self._issue_filter_user.new_responses()

        if _responses is not None:
            _header: str = f"Пользователь: {self._login}, новые задачи:"
            _str_responses: str = "\n".join(f'{format(_response, "log")}' for _response in _responses)
            logger.info("\n".join((_sep, _header, _str_responses, _sep)))
            _message_converter_user: _MessageTextUser = _MessageTextUser(self._login, _responses)

        else:
            _message_converter_user: _MessageTextUser = _MessageTextUserEmpty()

        return _message_converter_user.message_text(is_init=is_init)

    def to_json(self) -> tuple[str, list[str]]:
        return self._login, self._issue_filter_user.joined_issues()
