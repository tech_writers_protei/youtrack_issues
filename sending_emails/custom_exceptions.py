class BaseError(Exception):
    """Base error class to inherit."""


class MailServerRunError(BaseError):
    """The mail server has failed to run."""


class MessageGenerationError(BaseError):
    """The messages have not been created properly."""


class RequiredAttributeMissingError(BaseError):
    """Required attribute has no specified value."""


class NoResponseError(BaseError):
    """The response must be specified."""


class NoAuthTokenError(BaseError):
    """No environment variable named AUTH_TOKEN is found."""
