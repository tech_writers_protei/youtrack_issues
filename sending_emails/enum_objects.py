from email.message import EmailMessage
from enum import Enum
from functools import cached_property
from os import environ
from typing import TypeAlias, NamedTuple

from sending_emails.custom_exceptions import NoAuthTokenError

OptStr: TypeAlias = str | None
OptBytes: TypeAlias = bytes | None

_auth_token: str = environ.get("AUTH_TOKEN", None)

if _auth_token is None:
    raise NoAuthTokenError("Не найден AUTH_TOKEN")

_headers_request: dict[str, str] = {
    "Authorization": " ".join(("Bearer", _auth_token)),
    "Accept": "application/json",
    "Content-Type": "application/json",
}

_sep: str = "----------------------------------------"


def _params(login: str) -> tuple[tuple[str, str], ...]:
    _states_not_completed: str = ",".join(("Active", "Discuss", "New", "Paused", "Review"))
    _assignees_project: tuple[str, ...] = (
        "Исполнители_Doc", "Исполнители_Doc_ST", "Исполнители_Archive", "Исполнители_Archive_ST")
    _assignees: str = " OR ".join([f"{assignee}: {login}" for assignee in _assignees_project])
    _params_assignees: str = f"({_assignees})"
    _params_state: str = f"State: {_states_not_completed}"
    _params_field: str = ",".join(
        ("idReadable", "summary",
         "customFields(value,value(name),value(name,login),projectCustomField(field(name)))")
    )
    _params_query: str = " AND ".join((_params_state, _params_assignees))

    return (
        ("fields", _params_field),
        ("query", _params_query),
        ("customFields", "State"),
        ("customFields", "Дедлайн"),
        ("customFields", "Priority"),)


class CustomPort(Enum):
    HTTP = 80
    HTTPS = 443

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class CustomScheme(Enum):
    HTTP = "http"
    HTTPS = "https"

    def __str__(self):
        return f"{self.__class__.__name__}.{self._value_}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self._name_}>"


class MessageAttributes(NamedTuple):
    """
    Specifies the email message headers:\n
        From, Subject, Content-Type, Content-Transfer-Encoding, Content-Language, MIME-Version, Return-Path;
    """
    from_: str = None
    subject_: str = None
    content_type_: str = None
    content_transfer_encoding_: str = None
    content_language_: str = None
    mime_version_: str = None
    return_path_: str = None

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict()}"

    def prepare_values(self) -> dict[str, str]:
        return {k: v for k, v in self._asdict().items() if v is not None}

    @classmethod
    def from_message_kwargs(cls, message_kwargs: dict[str, str]):
        cls(
            message_kwargs["from_"],
            message_kwargs["subject_"],
            message_kwargs["content_type_"],
            message_kwargs["content_transfer_encoding_"],
            message_kwargs["content_language_"],
            message_kwargs["mime_version_"],
            message_kwargs["return_path_"])


class MessageYoutrack:
    _keys: tuple[str] = (
        "From",
        "Subject",
        "Content-Type",
        "Content-Transfer-Encoding",
        "Content-Language",
        "MIME-Version",
        "Return-Path")
    _users: dict[str, str] = {
        "bochkova": "Софья Бочкова",
        "brovko": "Максим Бровко",
        "chursin": "Алексей Чурсин",
        "demyanenko": "Светлана Демьяненко",
        "fesenko": "Виктор Фесенко",
        "kuksina": "Ольга Куксина",
        "lyalina": "Кристина Лялина",
        "matyushina": "Вероника Мозглякова",
        "mazyarova": "Дарья Мазярова",
        "nikitina": "Наталья Никитина",
        "sobolev-p": "Павел Соболев",
        "vykhodtsev": "Алексей Выходцев",
        "tarasov-a": "Андрей Тарасов",
        "rotmanova": "Ольга Ротманова",
        "andrianova": "Валерия Андрианова",
        "bozhok": "Влада Божок",
        "sergeev-d": "Дмитрий Сергеев",
        "sukhova": "Лидия Сухова",
        "babenko-v": "Валентин Бабенко",
        "safonova": "Софья Сафонова",
        "mikhailova-m": "Марина Михайлова"
    }

    def __init__(
            self,
            sender_email: OptStr,
            recipient_email: OptStr,
            message_text: OptStr,
            message_kwargs: MessageAttributes | None):
        if message_text is None:
            sender_email = None
            recipient_email = None
            message_kwargs = None
        if message_kwargs is None:
            _message_kwargs = dict()
        else:
            _message_kwargs = message_kwargs.prepare_values()
        self.sender_email: str = sender_email
        self.recipient_email: str = recipient_email
        self.message_text: str = message_text
        self.message_kwargs: dict[str, str] = _message_kwargs

    def __repr__(self):
        return f"<{self.__class__.__name__}(sender_email={self.sender_email}, recipient_email={self.recipient_email}," \
               f" message_text={self.message_text}, " \
               f"message_kwargs={MessageAttributes.from_message_kwargs(self.message_kwargs)})>"

    def __str__(self):
        return f"Сообщение\n{self.readable}"

    @cached_property
    def _recipient_login(self) -> str:
        return self.recipient_email.split("@")[0]

    @cached_property
    def recipient_name(self) -> str:
        return self._users[self._recipient_login]

    def message(self) -> OptBytes:
        message: EmailMessage = EmailMessage()
        message.set_payload(self.message_text)

        for k, v in zip(self._keys, tuple(self.message_kwargs.values())):
            message[k] = v

        message["To"] = self.recipient_email

        return message.as_string().encode("utf-8")

    @property
    def readable(self) -> OptStr:
        return self.message().decode("utf-8")


class MessageYoutrackEmpty(MessageYoutrack):
    def __init__(self):
        super().__init__(None, None, None, None)

    def __repr__(self):
        return f"<{self.__class__.__name__}: NullObject>"

    def __str__(self):
        return f"{self.__class__.__name__}: Null Message"

    @cached_property
    def _recipient_login(self) -> OptStr:
        return None

    @cached_property
    def recipient_name(self) -> OptStr:
        return None

    def message(self) -> OptBytes:
        return None

    @property
    def readable(self) -> OptStr:
        return None
