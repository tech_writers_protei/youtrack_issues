from pathlib import Path

from loguru import logger
from json_file_handler import JSONFile


class _UtilityHandler:
    _instance = None
    _log_folder: Path = Path(__file__).parent.joinpath("logs")
    _name: str = "issues.json"
    _json_file: JSONFile = JSONFile(_name)

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls._instance

    @classmethod
    def json_file_content(cls):
        json_file: JSONFile = JSONFile(cls._name)
        json_file.read()
        return json_file.content

    @classmethod
    def clear_json(cls):
        cls._json_file.content = cls.json_file_content()
        cls._json_file.nullify()
        logger.info(f"Файл {cls._name} очищен")

    @classmethod
    def do_nothing(cls):
        """Something like a placeholder."""
        logger.info("Тестовый запуск")

    @classmethod
    def clear_logs(cls):
        for file in cls._log_folder.iterdir():
            file.unlink()
            print(f"Лог-файл {file.name} удален")

    @classmethod
    def add_issue_to_all(cls, issue: str):
        cls._json_file.content = cls.json_file_content()

        for user in cls._json_file:
            cls._json_file[user].append(issue)

        logger.info(f"Задача {cls._name} добавлена всем пользователям")

    @classmethod
    def add_user(cls, user: str):
        cls._json_file.content = cls.json_file_content()
        cls._json_file[user] = []
        cls._json_file.write(cls._json_file.content)
        logger.info(f"Пользователь {user} добавлен")


if __name__ == "__main__":
    _UtilityHandler.clear_json()
